//
//  PoetryApp.swift
//  Poetry
//
//  Created by Adrianna Siwik on 04/07/2023.
//

import SwiftUI

@main
struct PoetryApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
